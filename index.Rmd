--- 
title: "UUtah 2018 Reproducibility Immersive Course"
author: "Vicky Steeves"
date: "June 2018"
description: "Material for the reproducibility immersion week at the University of Utah."
site: bookdown::bookdown_site
colorlinks: yes
favicon: /img/favicon.ico
documentclass: book
always_allow_html: yes
output:
  bookdown::gitbook:
    css: [style.css]
    config:
      toc:
        before: |
          <p><a href="http://campusguides.lib.utah.edu/UtahRR18/Course">2018 Reproducibility Immersive Course</a></p>
          <p>Instructor: <a href="https://vickysteeves.com">Vicky Steeves</a></p>
        after: |
          <p><a href="http://campusguides.lib.utah.edu/UtahRR18/Course">2018 Reproducibility Immersive Course</a></p>
          <p>Original content is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</p>
          <p><a href="https://gitlab.com/RLesur/bookdown-gitlab-pages" target="blank">Published with bookdown &amp; GitLab</a></p>
      edit: https://gitlab.com/VickySteeves/2018-uutah-repro/edit/master/%s
      download: ["pdf", "epub"]
      sharing:
        facebook: no
        twitter: yes
        google: no
        linkedin: no
        weibo: no
        instapper: no
        vk: no
  bookdown::pdf_book:
    latex_engine: pdflatex
  bookdown::epub_book: default
---

# Welcome! {-}
| **Date**: Monday-Thursday, June 11-14, 2018 10am-4pm & Conference Friday
| **Location**: Carolyn Tanner Irish Humanities Building Auditorium, University of Utah
| **Cost**: FREE; CME available

This 5-day course provides an opportunity to explore issues of research reproducibility in-depth in a seminar-type setting, followed by hands-on sessions to learn actionable, practical solutions to make your own work more reproducible. You can choose to attend some or all of the sessions. The final day of the workshop is the conference [Building Research Integrity through Reproducibility](http://campusguides.lib.utah.edu/UtahRR18/Conference) which requires a separate registration.

## About the course {-}
Various fields in the natural and social sciences face a ‘crisis of confidence’. Broadly, this crisis amounts to a pervasiveness of non-reproducible results in the published literature. For example, in the field of biomedicine, Amgen published findings that out of 53 landmark published results of pre-clinical studies, only 11% could be replicated successfully. This crisis is not confined to biomedicine. Areas that have recently received attention for non-reproducibility include biomedicine, economics, political science, psychology, as well as philosophy. Some scholars anticipate the expansion of this crisis to other disciplines.

This course explores the state of reproducibility. After giving a brief historical perspective, case studies from different disciplines (biomedicine, psychology, and philosophy) are examined to understand the issues concretely. Subsequently, problems that lead to non-reproducibility are discussed as well as possible solutions and paths forward.

## Course Outline {-}

<div class="sched">
| Day | Morning Sessions<br/> 10AM-Noon<br/>  Instructor: Hamid Seyedsayamdost | Afternoon Sessions<br/>  1PM-4PM<br/>  Instructor: [Vicky Steeves](https://vickysteeves.com) |
|---------|------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------|
| June 11 | Introduction: The Problem of Non-Reproducibility <br/> Reproducibility: A Historical Perspective <br/> State of Reproducibility: Bad Apples or Systemic Problems | [Git](version-control.html) <br/>  [GitLab/GitHub](version-control.html#hosting-platforms) |
| June 12 | Biomedicine<br/>  Psychology<br/> Philosophy | [OpenRefine](cleaning-data.html) <br/>  [R (including RStudio)](r-rstudio.html) |
| June 13 | Statistical Methods<br/>  Fraud<br/>  Incentives, Transparency, and Accountability | [Jupyter Notebooks](jupyter-notebooks.html) <br/>  [ReproZip + VisTrails](reprozip.html) |
| June 14 | Solutions Part I – Various Proposals <br/> Solutions – Part II – Replications: Problems and Promise <br/> Problems of Implementing Solutions | [OSF](the-open-science-framework.html) |
| June 15 | [Building Research Integrity through Reproducibility](http://campusguides.lib.utah.edu/UtahRR18/Conference) |
</div>